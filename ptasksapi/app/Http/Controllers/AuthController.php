<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Authentication Controller
 *
 * @copyright  TKT inc
 * @license    TKT  
 * @version    Release: beta v0.1
 * @since      05/2020
 */
class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    /**
     * Register
     *
     * @param username,password   
     * 
     * @author Khang
     * @return Response,UserInfo,AuthToken
     */
    public function register(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|min:4|max:20|unique:users',
            'password' => 'required|min:6|max:20',
        ]);
        // $temp= User::where('username',$request["username"])->first();
        // if($temp)
        // {
        //     return response()->json([
        //         'success' => false,
        //         'message' => "Username is duplicated"
        //     ], 409);
        // }
        $user = new User();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();


        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }



     /**
     * Login
     *
     * @param username,password   
     * 
     * @author Khang
     * @return Response,AuthToken,UserInfo
     */
    public function login(Request $request)
    {
        $input = $request->only('username', 'password');
        $jwt_token = null;
        $temp = User::where('username', $input["username"])->first();
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Name or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
            'user' => $temp
        ]);
    }


     /**
     * Logout
     * Disable token
     *
     * @param AuthToken;  
     * 
     * @author Khang
     * @return Response
     */
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    // public function getAuthUser(Request $request)
    // {
    //     $this->validate($request, [
    //         'token' => 'required'
    //     ]);

    //     $user = JWTAuth::authenticate($request->token);

    //     return response()->json(['user' => $user]);
    // }
}
