<?php

namespace App\Http\Controllers;

use App\Category;
use App\SharedList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;


/**
 * Category Controller
 *
 * @copyright  TKT inc
 * @license    TKT  
 * @version    Release: beta v0.1
 * @since      05/2020
 */
class CategoryController extends Controller
{

    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Get all categories of user by using Jwt Token
     *
     * @param AuthToken  
     * 
     * @author Khang
     * @return CategoryList
     */
    public function index()
    {
        return $this->user
            ->categories()->orderBy("categories.created_at", "desc")
            ->get(['id', 'name', 'shared_code','count_tasks','fail_tasks','success_tasks'])
            ->toArray();
    }

    /**
     * Get all shared's categories by using Jwt Token
     *
     * @param AuthToken  
     * 
     * @author Khang
     * @return CategoryList
     */
    public function indexShared()
    {
        return $this->user
            ->shared_lists()
            ->join('categories', 'categories.id', '=', 'shared_lists.category_id')
            ->orderBy("shared_lists.created_at", "desc")
            ->get(['categories.name', 'shared_lists.*'])
            ->toArray();
    }

    /**
     * Store a newly created category in storage.
     *
     * @param AuthToken  
     * 
     * @author Khang
     * @return CategoryList
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:5',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->shared_code = Str::random(60);


        if ($this->user->categories()->save($category))
            return response()->json([
                'success' => true,
                'category' => $category
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category could not be added'
            ], 500);
    }


    /**
     * Add a newly user in category .
     *
     * @param shared_code,category_id,AuthToken
     * 
     * @author Khang
     * @return Status
     */
    public function storeShared(Request $request)
    {

        $shardList = new SharedList();
        // $shardList->shared_code = $request->shared_code;
        $category = Category::where('categories.shared_code', '=', $request->shared_code)->where('categories.user_id', '!=', $this->user->id)->select('id')->first();

        $existCategory = SharedList::where('shared_lists.category_id', '=', $category->id)->where('shared_lists.user_id', '=', $this->user->id)->select('id')->first();
        if ($existCategory) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category exsisted'
            ], 406);
        }
        if ($category) {
            // return $this->user->id;
            $shardList->user_id = $this->user->id;
            $shardList->category_id = $category->id;
            $shardList->crud_permission = "rx";
            $shardList->save();
            return $shardList;
        }
        // $shardList->user_id = $request->user_id;
        // $shardList->crud_permission=$request->crud_permission;
        // $shardList->save();
        // if ($this->user->categories()->save($category))
        //     return response()->json([
        //         'success' => true,
        //         'category' => $category
        //     ]);
        // else
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Sorry, category could not be added'
        //     ], 500);
        return response()->json([
            'success' => false,
            'message' => 'Sorry, category exsisted'
        ], 406);
    }



    /**
     * Display the specified category.
     *
     * @param category_id,AuthToken
     * 
     * @author Khang
     * @return CategoryList
     */

    public function show($id)
    {
        $category = $this->user->categories()->select('categories.shared_code', 'categories.user_id')->find($id);

        if (!$category) {
            $result = DB::table('shared_lists')
                ->join('categories', 'categories.id', '=', 'shared_lists.category_id')->where('shared_lists.category_id', '=', $id)
                ->where('shared_lists.user_id', '=', $this->user->id)

                ->select('categories.shared_code', 'shared_lists.*')->get();

            if ($result) {
                return $result;
            }



            return response()->json([
                'success' => false,
                'message' => 'Sorry, category with id ' . $id . ' cannot be found'
            ], 400);
        }

        return $category;
    }


    /**
     * Update the specified category in storage (category's name).
     *
     * @param category_id,AuthToken,categoryName
     * 
     * @author Khang
     * @return Status
     */
    public function update(Request $request, $id)
    {
        $category = $this->user->categories()->find($id);

        if (!$category) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category with id ' . $id . ' cannot be found'
            ], 400);
        }

        $updated = $category->fill($request->all())
            ->save();

        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category could not be updated'
            ], 500);
        }
    }

    /**
     * Remove the specified category from storage.
     *
     * @param category_id,AuthToken
     * 
     * @author Khang
     * @return Status
     */
    public function destroy($id)
    {
        $category = $this->user->categories()->find($id);

        if (!$category) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category with id ' . $id . ' cannot be found'
            ], 400);
        }

        if ($category->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'category could not be deleted'
            ], 500);
        }
    }

    /**
     * Get all user in shared list.
     *
     * @param category_id,AuthToken
     * 
     * @author Khang
     * @return UserList
     */
    public function indexAllUserInSharedCate(Request $request, $category_id)
    {
        $result = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->join('users', 'users.id', '=', 'shared_lists.user_id')->select(['users.username', 'users.id', 'shared_lists.crud_permission']);
        return $result->get();
    }

    /**
     * Remove user in shared list.
     *
     * @param category_id,userId,AuthToken
     * 
     * @author Khang
     * @return Status
     */
    public function destroyUserInSharedCate(Request $request, $category_id)
    {
        $userId = $request->input('userId');

        $result = DB::table('categories')->where('categories.user_id', '=', $this->user->id)
            ->find($category_id);
        if ($result) {
            $destroyResult = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->where('shared_lists.user_id', '=', $userId)->delete();
            if ($destroyResult)
                return response()->json([
                    'success' => true
                ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'user could not be deleted'
        ], 500);;
    }


    /**
     * Update role user in shared list.
     *
     * @param category_id,userId,AuthToken
     * 
     * @author Khang
     * @return Status
     */
    public function updateUserInSharedCate(Request $request, $category_id)
    {
        $userId = $request->input('userId');
        // $permission = $request->input('permission');

        $result = DB::table('categories')->where('categories.user_id', '=', $this->user->id)
            ->find($category_id);
        if ($result) {
            $data = array('crud_permission' => $request->input('permission'));

            $updateResult = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->where('shared_lists.user_id', '=', $userId)->update($data);
            if ($updateResult)
                return response()->json([
                    'success' => true
                ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'user could not be updated'
        ], 500);;
    }
}
