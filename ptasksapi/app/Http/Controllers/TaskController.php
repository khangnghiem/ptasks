<?php

namespace App\Http\Controllers;

use App\Category;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Category TaskController
 *
 * @copyright  TKT inc
 * @license    TKT  
 * @version    Release: beta v0.1
 * @since      05/2020
 */
class TaskController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }


    /**
     * Get all task from categories id
     *
     * @param request,category_id,AuthToken
     * 
     * @author Tuan
     * @return TaskList
     */
    public function index(Request $request, $category_id)
    {

        $searchString = $request->input('searchString');
        $sortByWeek = $request->input('sortByWeek');
        $sortByMonth = $request->input('sortByMonth');
        $dateFrom = $request->input('dateFrom');
        $dateTo = $request->input('dateTo');
        $expired = $request->input('expired');

        try {
            $result = $this->user->hasMany(Category::class)
                ->find($category_id)->hasMany(Task::class)
                ->where('tasks.name', 'like', '%' . $searchString . '%');
        } catch (\Throwable $th) {
            $result = DB::table('shared_lists')
                ->where('tasks.category_id', '=', $category_id)
                ->join('tasks', 'tasks.category_id', '=', 'shared_lists.category_id')
                ->select('tasks.*');
        }


        if ($sortByWeek == "true") {
            $result = $result->where('tasks.date_from', '<=', now()->locale('vi')->endOfWeek())
                ->where('tasks.date_to', '>=', now()->locale('vi')->startOfWeek());
        }
        if ($sortByMonth == "true") {

            $result = $result->where('tasks.date_from', '<=', now()->locale('vi')->endOfMonth())
                ->where('tasks.date_to', '>=', now()->locale('vi')->startOfMonth());
        }
        if (!empty($dateFrom) && !empty($dateTo)) {

            $result = $result->where('tasks.date_from', '<=', $dateTo)
                ->where('tasks.date_to', '>=', $dateFrom);
        }

        if ($expired == "true") {
            $result = $result
                ->where('tasks.date_to', '<', now()->locale('vi'));
        }

        return $result->get();
    }

    /**
     * Get all task of user
     *
     * @param request,category_id,AuthToken
     * 
     * @author Tuan
     * @return TaskList
     */
    public function indexWithAllTasks(Request $request)
    {
        $searchString = Str::lower($request->input('searchString'));
        $sortByWeek = $request->input('sortByWeek');
        $sortByMonth = $request->input('sortByMonth');
        $date_from = $request->input('dateFrom');
        $date_to = $request->input('dateTo');
        if ($sortByWeek == "true") {
            $result = $this->user->hasMany(Category::class)
                ->join('tasks', 'tasks.category_id', '=', 'categories.id')
                ->select('tasks.*', 'categories.name as catename')
                ->where('tasks.date_to', '<', now()->locale('vi')->endOfWeek())
                ->get();
            return $result;
        }
        if ($sortByMonth == "true") {
            $result = $this->user->hasMany(Category::class)
                ->join('tasks', 'tasks.category_id', '=', 'categories.id')
                ->select('tasks.*', 'categories.name as catename')
                ->where('tasks.date_to', '<', now()->locale('vi')->endOfMonth())
                ->get();
            return $result;
        }
        if (!empty($date_from) && !empty($date_to)) {
            $result = $this->user->hasMany(Category::class)
                ->join('tasks', 'tasks.category_id', '=', 'categories.id')
                ->select('tasks.*', 'categories.name as catename')
                ->where('tasks.date_from', '<', $date_from)
                ->where('tasks.date_to', '>', $date_to)
                ->get();
            return $result;
        }

        return $this->user->hasMany(Category::class)
            ->join('tasks', 'tasks.category_id', '=', 'categories.id')
            ->select('tasks.*', 'categories.name as catename')
            ->where('tasks.name', 'like', '%' . $searchString . '%')
            ->get();
    }


    /**
     * Save task create by user
     *
     * @param request,category_id,AuthToken
     * 
     * @author Tuan
     * @return store status code
     */
    public function store(Request $request, $category_id)
    {

        $this->validate($request, [
            'name' => 'required|min:1',
            'priority' => 'required',
            'description' => 'required',
            'content' => 'required',
            'date_from' => 'required',
            'date_to' => 'required'
        ]);

        // $checkLow = Str::is($request->priority, 'low');
        // $checkLow = Str::is($request->priority, 'low');
        // $checkLow = Str::is($request->priority, 'low');

        if ($request->priority != 'low' && $request->priority != 'medium' && $request->priority != 'high') {
            return response()->json([
                'success' => false,
                'message' => $request->priority
            ], 500);
        }


        $task = new Task();
        $task->name = $request->name;
        $task->priority = $request->priority;
        $task->content = $request->content;
        $task->description = $request->description;
        $task->date_from = $request->date_from;
        $task->date_to = $request->date_to;
        $task->created_by_uname = $this->user->username;
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $sharedPermission = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->where('shared_lists.user_id', '=', $this->user->id)->first();
        try {
            if ($sharedPermission->crud_permission == "rw") {
                $data = array("name" => $task->name, "priority" => $task->priority, "content" => $task->content, "description" => $task->description, "date_from" => $task->date_from, "date_to" => $task->date_to, "category_id" => $category_id, "created_by_uname" => $task->created_by_uname, "created_at" => $task->created_at, "updated_at" => $task->updated_at);

                $resultFromIsSharedUser = DB::table('tasks')
                    ->where('tasks.category_id', '=', $category_id)
                    ->insert($data);
                if ($resultFromIsSharedUser) {
                    $updateTaskFromCate = Category::find($category_id);
                    $updateTaskFromCate->count_tasks += 1;
                    $updateTaskFromCate->save();
                    return response()->json([
                        'success' => true,
                        'task' => $task
                    ]);
                }
            }
        } catch (\Throwable $th) {
        }

        if ($this->user->tasks($category_id)->save($task)) {
            $updateTaskFromCate = Category::find($category_id);
            $updateTaskFromCate->count_tasks += 1;
            $updateTaskFromCate->save();

            return response()->json([
                'success' => true,
                'task' => $task
            ]);
        } else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be added'
            ], 500);
    }



    /**
     * Get user's task accroding to task id
     *
     * @param request,category_id,AuthToken
     * 
     * @author Tuan
     * @return task
     */
    public function show($category_id, Request $request)
    {
        $taskid = $request->input('taskid');
        $task = $this->user->task($category_id, $taskid);

        if (!$task) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task with id ' . $taskid . ' cannot be found'
            ], 400);
        }

        return $task;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'priority' => 'required',
            'description' => 'required',
            'content' => 'required',
            'date_from' => 'required',
            'date_to' => 'required'
        ]);

        $taskId = $request->input('taskid');
        try {
            $task = $this->user->tasks($category_id)->find($taskId);
        } catch (\Throwable $th) {
            $updatePermissionFromShared = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->where('shared_lists.user_id', '=', $this->user->id)->first();
            if ($updatePermissionFromShared->crud_permission == "rw") {
                $data = array(
                    'name' => $request->name, "priority" => $request->priority, "content" => $request->content, "description" => $request->description, "date_from" => $request->date_from, "date_to" => $request->date_to,
                    "category_id" => $category_id
                );
                $updateFromSharedResult = DB::table('tasks')->where('tasks.id', '=', $taskId)->update($data);
                if ($updateFromSharedResult) {
                    return response()->json([
                        'success' => true
                    ]);
                }
            }
        }
        if (!$task) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, category with id ' . $taskId . ' cannot be found'
            ], 400);
        }



        if ($request->priority != 'low' && $request->priority != 'medium' && $request->priority != 'high') {
            return response()->json([
                'success' => false,
                'message' => $request->priority
            ], 500);
        }

        $updated = $task->fill($request->all())
            ->save();

        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be updated'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $category_id,$task_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $category_id)
    {
        $taskId = $request->input('taskid');
        try {
            $task = $this->user->tasks($category_id)->find($taskId);
        } catch (\Throwable $th) {
            $deletePermissionFromShared = DB::table('shared_lists')->where('shared_lists.category_id', '=', $category_id)->where('shared_lists.user_id', '=', $this->user->id)->first();
            if ($deletePermissionFromShared->crud_permission == "rw") {

                $task = DB::table('tasks');


                if ($task->date_to < now()->locale('vi')) {
                    $updateTaskFromCate = Category::find($category_id);
                    $updateTaskFromCate->fail_tasks += 1;
                    $updateTaskFromCate->save();
                } else {
                    $updateTaskFromCate = Category::find($category_id);
                    $updateTaskFromCate->success_tasks += 1;
                    $updateTaskFromCate->save();
                }




                $deleteFromSharedResult = DB::table('tasks')->delete($taskId);




                if ($deleteFromSharedResult) {


                    return response()->json([
                        'success' => true
                    ]);
                }
            }
        }


        if (!$task) {

            return response()->json([
                'success' => false,
                'message' => 'Sorry, category with id ' . $taskId . ' cannot be found'
            ], 400);
        }

        if ($task->delete()) {
            if ($task->date_to < now()->locale('vi')) {
                $updateTaskFromCate = Category::find($category_id);
                $updateTaskFromCate->fail_tasks += 1;
                $updateTaskFromCate->save();
            } else {
                $updateTaskFromCate = Category::find($category_id);
                $updateTaskFromCate->success_tasks += 1;
                $updateTaskFromCate->save();
            }

            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'task could not be deleted'
            ], 500);
        }
    }
}
