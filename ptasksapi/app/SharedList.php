<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedList extends Model
{
    protected $fillable = [
        'id','category_id','user_id','crud_permission'
    ];
}
