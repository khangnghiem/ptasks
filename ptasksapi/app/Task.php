<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'id','name','priority','is_completed',
        'content','date_from','date_to','description','category_id'
    ];
    protected $appends = ['days_left'];

    public function getDaysLeftAttribute()
    {
        $datefrom = Carbon::now();
        $dateto = new Carbon($this->date_to);
        $diff_in_days= $datefrom->diffInDays($dateto);
        if($dateto <= $datefrom)
            return -$diff_in_days;
        return $diff_in_days;
    }
}
