<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password','display_name','email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function shared_lists()
    {
        return $this->hasMany(SharedList::class);
    }

    // public function getCategory($id)
    // {
    //     return $this->hasMany(Category::class)->where('id',$id)->first();
    // }
    
    public function tasks($id)
    {
        return $this->hasMany(Category::class)->find($id)->hasMany(Task::class);
    }



    public function tasksWithQuery($id,$searchString)
    {
        return $this->hasMany(Category::class)->where('id',$id)->first()->hasMany(Task::class)->where('name','like','%'.$searchString.'%')->orderBy('created_at','desc');
    }

    public function tasksWithQueryW($id,$searchString)
    {
        return $this->hasMany(Category::class)->where('id',$id)->first()->hasMany(Task::class)->where('tasks.name','like','%'.$searchString.'%')->join('categories','categories.id','=','tasks.category_id')->orderBy('created_at','desc')->select('tasks.*','categories.name as category_name');
    }
    // ->where('name','like','%'.$searchString.'%');
}
