<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('logout', 'AuthController@logout');
    
    Route::get('user', 'AuthController@getAuthUser');

    Route::post('categories/shared', 'CategoryController@storeShared');
    Route::get('categories/shared', 'CategoryController@indexShared');
    
    Route::get('categories', 'CategoryController@index');

    Route::get('categories/{id}', 'CategoryController@show');
    Route::post('categories', 'CategoryController@store');
    Route::put('categories/{id}', 'CategoryController@update');
    Route::delete('categories/{id}', 'CategoryController@destroy');


    Route::get('{category_id}/tasks', 'TaskController@index');
    Route::get('{category_id}/tasks/{task_id}', 'TaskController@show');
    Route::get('tasks/filter', 'TaskController@indexWithAllTasks');
    Route::post('{category_id}/tasks', 'TaskController@store');
    Route::put('{category_id}/tasks', 'TaskController@update');
    Route::delete('{category_id}/tasks', 'TaskController@destroy');

    Route::get('categories/{id}/users', 'CategoryController@indexAllUserInSharedCate');
    Route::delete('categories/{id}/users', 'CategoryController@destroyUserInSharedCate');
    Route::put('categories/{id}/users', 'CategoryController@updateUserInSharedCate');

});
