import React from "react";
import "./App.css";
import "./SignUp.css"
import Login from "./components/Login";
import Home from "./components/Home";
import Header from "./components/Header";

export const AuthContext = React.createContext();

const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.token
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null
      };
    case "RESTORE_TOKEN":

      return {
        ...state,
        token: action.token,
        isAuthenticated: true,
      };
    default:
      return state;
  }
};




export default function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  
  React.useEffect(() => {
    const restoreLogin = () => {
      let userToken;
      try {
        userToken = localStorage.getItem("token");
        console.log("bootstrapAsync -> userToken", userToken)
        if (userToken != null)
          dispatch({ type: "RESTORE_TOKEN", token: JSON.parse(userToken) });


      } catch (e) { }
    };

    restoreLogin();
  }, []);



  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    >

      <div className="App">{!state.isAuthenticated ? <Login /> :
        <>
          {/* <Header /> */}
          <Home />
        </>
      }
      </div>
    </AuthContext.Provider>
  );
}
//abc