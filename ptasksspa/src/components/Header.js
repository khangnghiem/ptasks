import React, { useRef } from "react";
import { AuthContext } from "../App";
import "../assets/tasks.css"
// import Search from "./Search"
import { Modal, Button } from 'react-bootstrap';
import { apiUrl } from "../env"
import moment from 'moment'
import "../assets/tasks.css"
import { css } from "@emotion/core";
import ScaleLoader from "react-spinners/ScaleLoader";
import { useHistory } from "react-router-dom";


const override = css`
  display: block;
  margin: 2 auto;
  border-color: red;
`;


export default function Header({ authToken }) {
  const { dispatch } = React.useContext(AuthContext);
  const history = useHistory();

  const [showSearchModal, setShowSearchSearchModal] = React.useState(false)
  const [searchString, setSearchString] = React.useState("")
  const [tasks, setTasks] = React.useState([])
  const [loading, setLoading] = React.useState(false)



  const keyPress = (e) => {
    setTasks([])

    if (e.keyCode == 13 && e.target.value != "") {
      setSearchString(e.target.value)
      return setShowSearchSearchModal(true)
    }
  }




  const getTasks = async () => {

    setLoading(true)
    await fetch(`${apiUrl}/tasks/filter?searchString=${searchString}`, {
      method: "get",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${authToken}`
      }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        setTasks(resJson)
        setLoading(false)
      })
      .catch(error => {
        console.log(error);

      });
  }


  React.useEffect(() => {

  }, [searchString])


  function createMarkup(html) {
    return { __html: html };
  }


  return (

    <>

      <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">

        <a className="navbar-brand col-md-2 col-lg-2 mr-0 px-3" href="#">PTASKS Xin chào, {JSON.parse(localStorage.getItem('user')).username}</a>
        <button className="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <input className="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search" onKeyDown={keyPress} />
        <ul className="navbar-nav px-3">
          <li className="nav-item text-nowrap signout">
            <a className="nav-link" onClick={() => {
              dispatch({
                type: "LOGOUT",

              })
              history.push("/");
            }}>Sign out</a>
          </li>
        </ul>

      </nav>
      <main>
        <Modal
          size="lg"
          centered
          dialogClassName="modal-90w"
          show={showSearchModal}
          onShow={() => { getTasks() }}
          onHide={() => {
            setShowSearchSearchModal(false)

          }}>
          <Modal.Header closeButton>
            <Modal.Title>Result for "{searchString}"</Modal.Title>
          </Modal.Header>
          <Modal.Body >

            <div className="sweet-loading" align="center">
              <ScaleLoader
                css={override}
                size={50}
                width={10}
                height={45}
                radius={5}
                color={"#f7310a"}
                loading={loading}
              />
            </div>
            <div className="row d-flex justify-content-center">
              {tasks.length > 0 &&
                tasks.map(task => (
                  <div className={'mb-3 ml-2 card'} key={task.id}>
                    <h5 className={"card-header " + (task.priority == "high" ? 'bg-danger text-white' : "")
                      + (task.priority == "medium" ? 'bg-warning text-white' : 'bg-success text-white')}>
                      {task.name} ( Category: {task.catename} )</h5>
                    <div className="card-body">
                      <div className="mb-3">
                        <p className="card-text">{task.description}</p>
                      </div>
                      <button className="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target={'#' + task.id + "x"} aria-expanded="false" aria-controls={task.id + "x"}>Details</button>
                      {/* <button className="btn btn-outline-success ml-2 btn-sm" onClick={() => { }}>Edit</button> */}
                      {/* <button className="btn btn-outline-danger ml-2 btn-sm" >Delete</button> */}
                      {/* <button className="btn btn-outline-danger ml-2 btn-sm" onClick={() => { }}>Remove Task</button> */}
                      <div className="collapse" id={task.id + "x"} >
                        <div className="card-header mt-3 bg-light border" dangerouslySetInnerHTML={createMarkup(task.content)} >
                        </div>
                      </div>
                    </div>
                    <div className="card-footer bg-transparent border-secondary">{moment(task.date_from).format('LLLL')} ({moment(task.date_from).fromNow()})</div>
                  </div>
                ))}
            </div>
            {tasks.length == 0 && loading == false && <div className="text-center" align="center"><h2>No result found !!!</h2></div>}



          </Modal.Body>
          <Modal.Footer>

            <Button variant="secondary" onClick={() => {
              setShowSearchSearchModal(false)
            }}>
              Close
            </Button>

          </Modal.Footer>
        </Modal>
      </main>
    </>
  );
};