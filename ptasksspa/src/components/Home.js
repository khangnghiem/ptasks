import React from "react";
import { AuthContext } from "../App";
import "../assets/dashboard.css";
import { apiUrl } from "../env"
import { message } from 'antd';
import { useForm } from "react-hook-form";
import { Modal, Button } from 'react-bootstrap';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useLocation
} from "react-router-dom";
import Tasks from "./Tasks"
import Header from "../components/Header";
import Info from "./Info"
import "../assets/tasks.css"
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import SharedUser from '../components/SharedUser'
import { useHistory } from "react-router-dom";

// function useQuery() {
//   return new URLSearchParams(useLocation().search);
// }

export default function Home() {
  return (


    <Router>
      <App />
    </Router>

  );
}

function App() {
  const history = useHistory();

  const { errors, handleSubmit, register, setValue, watch } = useForm();
  const { state: authState } = React.useContext(AuthContext);
  const [categories, setCategories] = React.useState([]);
  const [sharedCategories, setSharedCategories] = React.useState([]);
  const [show, setShow] = React.useState(false);
  const [refetch, setRefetch] = React.useState(false);
  // const [currentCategory, setCurrentCategory] = React.useState("");
  const [editState, setEditState] = React.useState(false);
  const [cateId, setCateId] = React.useState(0);
  // const [editGroup, setEditGroup] = React.useState(0);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const [showShared, setShowShared] = React.useState(false);
  const handleShowSharedCategories = () => {
    setShowShared(true)
  }
  const handleCloseSharedCategories = () => {
    setShowShared(false)
  }
  const [sharedCode, setSharedCode] = React.useState("");

  const getCategories = async () => {
    await fetch(`${apiUrl}/categories`, {
      headers: {
        Authorization: `Bearer ${authState.token}`
      }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        setCategories(resJson)
      })
      .catch(error => {
        console.log(error);

      });
  }

  const getSharedCategories = async () => {
    await fetch(`${apiUrl}/categories/shared`, {
      headers: {
        Authorization: `Bearer ${authState.token}`
      }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        setSharedCategories(resJson)
      })
      .catch(error => {
        console.log(error);

      });
  }

  const addCategories = async (data) => {
    console.log("addCategories -> data", data)

    let cateTemp = {};
    cateTemp["name"] = data.name;

    await fetch(`${apiUrl}/categories`, {
      method: "post",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${authState.token}`
      },
      body: JSON.stringify(cateTemp)
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        setCategories(prevState => [resJson.category, ...prevState])
        setValue("name", "")
      })
      .catch(error => {
        console.log(error);
      });
  }

  const editCategories = async (data) => {
    let categoryTemp = {};
    categoryTemp["id"] = cateId;
    categoryTemp["name"] = data.name;

    await fetch(`${apiUrl}/categories/${cateId}`, {
      method: "put",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${authState.token}`
      },
      body: JSON.stringify(categoryTemp)
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        setValue("name", "")

        setCategories(prevState => {
          const category = prevState.filter(category => category.id == cateId);
          let index = prevState.indexOf(category[0]);
          prevState[index] = categoryTemp;
          return prevState;
        }
        );
        setEditState(false)

      })
      .catch(error => {
        console.log(error);

      });
  }

  const removeCategory = async (cateID) => {
    // console.log("removeCategory -> cateID", cateID)
    await fetch(`${apiUrl}/categories/${cateID}`, {
      method: "DELETE",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${authState.token}`
      },
    })
      .then(res => {
        if (res.ok) {
          message.success('Ok');
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        console.log("Tasks -> resJson", resJson);
        // setCategories(prevState => [resJson.category, ...prevState])
        setCategories(prevState => {
          const categories = prevState.filter(category => category.id !== cateID);
          return categories;
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  const addSharedCategoires = async (sharedCode) => {
    await fetch(`${apiUrl}/categories/shared`, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${authState.token}`
      },
      body: JSON.stringify({ "shared_code": sharedCode }),
    })
      .then(res => {
        if (res.ok) {
          message.success('Ok');
          setRefetch(!refetch)
          handleCloseSharedCategories()
          return res.json();
        } else {
          message.error('Fail');
          handleCloseSharedCategories()
          throw res;
        }
      })
      .then(resJson => {
        console.log("resJson", resJson)

      })
      .catch(error => {
        console.log(error);
      });
  }







  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  React.useEffect(() => {
    getCategories()
    getSharedCategories()
  }, [refetch])

  let query = useQuery();

  return (
    <>
      <Header authToken={authState.token} />
      <div className="container-fluid">
        <div className="row">
          <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse" aria-expanded="false">
            <div className="sidebar-sticky pt-3">
              <Accordion defaultActiveKey="0">
                <ul className="nav flex-column">
                  <li className="nav-item">
                    <div className="d-flex justify-content-between mr-3">
                      <Link className="nav-link active" to="/">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
              Task Categories <span className="sr-only">(current)</span>
                      </Link>
                      <div class="d-flex align-content-center flex-wrap">
                        <Button onClick={handleShow} className="mr-1" variant="outline-dark" size="sm">
                          <svg class="bi bi-plus-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                            <path fillRule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                            <path fillRule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                          </svg>
                        </Button>
                        <Accordion.Toggle as={Button} eventKey="0" variant="outline-primary" size="sm">
                          <svg class="bi bi-caret-down-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
                          </svg>
                        </Accordion.Toggle>
                      </div>
                    </div>
                  </li>
                  <Accordion.Collapse eventKey="0">
                    <div>
                      {categories.length > 0 &&
                        categories.map(category => (
                          <li className="nav-item" key={category.id.toString()} onClick={() => { }}>
                            <Link to={'/' + category.id + "?catename=" + category.name} className="nav-link"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                              {category.name}</Link>

                          </li>
                        ))}
                    </div>
                  </Accordion.Collapse>
                </ul>
              </Accordion>
              <div class="dropdown-divider shadow-sm"></div>
              <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Shared Categories</span>
                <Button onClick={handleShowSharedCategories} variant="outline-dark" size="sm">
                  <svg class="bi bi-file-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9 1H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V8h-1v5a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h5V1z" />
                    <path fill-rule="evenodd" d="M13.5 1a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V1.5a.5.5 0 0 1 .5-.5z" />
                    <path fill-rule="evenodd" d="M13 3.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z" />
                  </svg>
                </Button>
                {/* <svg onClick={handleShow} className="d-flex align-items-center text-muted" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg> */}
              </h6>

              {/* category modal */}
              <Modal size="lg" show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                  <Modal.Title>Categories</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div className="mb-3">
                    {
                      editState ?
                        // edit category
                        <form className="input-group mb-3" onSubmit={handleSubmit(editCategories)}>
                          <input className="name" type="text" className={'form-control ' + (errors.name ? ' is-invalid' : '')} ref={register({ required: true, minLength: 5, })} />
                          <button
                            type="submit"
                            className="ml-2 btn btn-warning">Confirm</button>
                          <button className='ml-2 btn btn-secondary' onClick={() => {
                            setEditState(!editState)
                            setValue("name", "")
                          }}>Cancel</button>
                          {
                            errors.name &&
                            <div name="name" className="invalid-feedback" >
                              Required
                            </div>
                          }
                        </form>
                        :
                        // add category
                        <>
                          <form className="input-group mb-3" onSubmit={handleSubmit(addCategories)}>
                            <input name="name" type="text" className={'form-control ' + (errors.name ? ' is-invalid' : '')} ref={register({ required: true, minLength: 5, })} />
                            <button type="submit" className="ml-2 btn btn-success">Add</button>
                            {
                              errors.name &&
                              <div name="name" className="invalid-feedback" >
                                Required
                            </div>
                            }
                          </form>

                        </>

                    }
                  </div>

                  <div className="my-custom-scrollbar">

                    <table className="table table-hover table-bordered" >

                      <thead>
                        <tr>
                          <td scope="col"><strong>Name</strong></td>
                          <td scope="col" align="center"><strong>Function</strong></td>
                        </tr>
                      </thead>
                      <tbody>

                        {categories.length > 0 && categories.map(category => (
                          <tr key={category.id.toString()}>
                            <td scope="row" className="col-table">{category.name}</td>
                            <td scope="row" align="center">
                              <button
                                type="button"
                                disabled={editState}
                                className="btn btn-warning ml-2"
                                onClick={() => {

                                  setEditState(!editState);
                                  setValue("name", category.name);
                                  setCateId(category.id)

                                }}>Edit</button>
                              <button
                                type="button"
                                disabled={editState}
                                className="btn btn-danger ml-2"
                                onClick={() => {
                                  removeCategory(category.id)
                                }}>Remove</button>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>

              {/* Shared Categories */}
              <Modal size="lg" show={showShared} onHide={handleCloseSharedCategories} centered>
                <Modal.Header closeButton>
                  <Modal.Title>Shared Categories</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                  <div className="form-group">
                    <label htmlFor="sharedCode" >Add a shared code</label>
                    <input className="form-control" value={sharedCode} onChange={(event) => {
                      setSharedCode(event.target.value)
                    }} name="sharedCode" placeholder="Code...." />
                  </div>
                  <input className="btn btn-primary" value="Ok" onClick={() => {
                    addSharedCategoires(sharedCode);
                  }} />

                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleCloseSharedCategories}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>


              <ul className="nav flex-column mb-2">

                {sharedCategories.length > 0 &&
                  sharedCategories.map(sharedcategory => (
                    <li className="nav-item" key={sharedcategory.category_id.toString()} onClick={() => { }}>
                      <Link to={'/' + sharedcategory.category_id + "?catename=" + sharedcategory.name} className="nav-link"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        {sharedcategory.name}</Link>
                    </li>
                  ))}
                {/* <li className="nav-item">
                  <a className="nav-link" href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                    Current month
                  </a>
                </li> */}

              </ul>
            </div>
          </nav>

          <Switch>
            <Route exact path="/" children={<Info authToken={authState.token}/>} />

            <Route exact path="/:id" children={<Tasks authToken={authState.token} catename={query.get('catename')} />} />
            <Route exact path="/users/:id" children={<SharedUser authToken={authState.token} catename={query.get('catename')} />} />
          </Switch>


        </div>


      </div>
    </>
  );
}




