import React from "react";

import { Bar } from "react-chartjs-2";
import { apiUrl } from "../env"


export default function Info({ authToken }) {

  const [categories, setCategories] = React.useState([]);

  const getCategories = async () => {
    await fetch(`${apiUrl}/categories`, {
      headers: {
        Authorization: `Bearer ${authToken}`
      }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then(resJson => {
        let labelsTemp = [];
        let success_tasks = [];
        let fail_tasks = [];
        let count_tasks = [];
        resJson.forEach(category => {
          labelsTemp.push(category.name)
          success_tasks.push(category.success_tasks)
          fail_tasks.push(category.fail_tasks)
          count_tasks.push(category.count_tasks)
        });

        let chartData = {
          labels: labelsTemp,
          datasets: [
            {
              label: "Succeed task",
              backgroundColor: "rgba(105, 224, 86, 0.9)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 1,
              data: success_tasks
            },
            {
              label: "Failed tasks",
              backgroundColor: "rgba(255,99,132,0.2)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 1,
              data: fail_tasks
            },
            {
              label: "Total tasks",
              backgroundColor: "rgb(144, 144, 247)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 1,
              data: count_tasks
            }
          ]

        }

        setData(chartData)
        setCategories(resJson)
      })
      .catch(error => {
        console.log(error);

      });
  }

  const [data, setData] = React.useState({});
  const options = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          stepSize: 1
        }
      }]
    },
    type: "bar"
    //   scales: {
    //     xAxes: [{
    //         stacked: true
    //     }],
    //     yAxes: [{
    //         stacked: true
    //     }]
    // }
  };
  const setDataForChart = () => {

    let labelsTemp = [];
    let success_tasks = [];
    let fail_tasks = [];
    let count_tasks = [];
    categories.forEach(category => {
      labelsTemp.push(category.name)
      success_tasks.push(category.success_tasks)
      fail_tasks.push(category.fail_tasks)
      count_tasks.push(category.count_tasks)
    });

    let chartData = {
      labels: labelsTemp,
      datasets: [
        {
          label: "Succeed task",
          backgroundColor: "rgba(105, 224, 86, 0.9)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          data: success_tasks
        },
        {
          label: "Failed tasks",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          data: fail_tasks
        },
        {
          label: "Total tasks",
          backgroundColor: "rgb(144, 144, 247)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          data: count_tasks
        }
      ]

    }
    
    setData(chartData)
  }

  React.useEffect(() => {
    getCategories();
    // setDataForChart();
  }, [])




  return (
    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">

      <div className="row mt-2">

        <Bar
          data={data}
          width={null}
          height={null}
          options={options}
        />
      </div>
      <table class="table table-striped mt-2 mb-2">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Category name</th>
            <th scope="col">Succeed tasks</th>
            <th scope="col">Fail tasks</th>
            <th scope="col">Total</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category, index) => {
            return (
              <tr>
                <th scope="row">{index+1}</th>
                <td>{category.name}</td>
                <td>{category.success_tasks}</td>
                <td>{category.fail_tasks}</td>
                <td>{category.count_tasks}</td>
              </tr>
            );
          })}

        </tbody>
      </table>


    </main>

  );
}