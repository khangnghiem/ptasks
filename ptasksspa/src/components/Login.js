import React from "react";
import { AuthContext } from "../App";
import Register from "./Register"
import { apiUrl } from "../env"
import { useHistory } from "react-router-dom";

export const Login = () => {
  const history = useHistory();

  const [registerState, setRegisterState] = React.useState(false)
  const { dispatch } = React.useContext(AuthContext);
  const initialState = {
    username: "",
    password: "",
    isSubmitting: false,
    errorMessage: null
  };
  const [data, setData] = React.useState(initialState);
  const handleInputChange = event => {
    setData({
      ...data,
      [event.target.name]: event.target.value
    });
  };
  const handleFormSubmit = event => {
    event.preventDefault();
    setData({
      ...data,
      isSubmitting: true,
      errorMessage: null
    });
    fetch(`${apiUrl}/login`, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: data.username,
        password: data.password
      })
    })
      .then(res => {
        if (res.ok) {
          
          return res.json();
          
        }
        throw res;
      })
      .then(resJson => {
        dispatch({
          type: "LOGIN",
          payload: resJson

        })
        history.push("#");
      })
      .catch(error => {
        setData({
          ...data,
          isSubmitting: false,
          errorMessage: error.message || error.statusText
        });
      });
  };
  return (
    // <div className="login-container">
    //   <div className="container border shadow rounded-lg bg-light">
    //     {!registerState ? <form onSubmit={handleFormSubmit} className="">
    //       <h3 align="center">Task management</h3>
    //       <div class="form-group">
    //         <label className="font-weight-bold">Username</label>
    //         <input class="form-control pl-2" type="text"
    //           value={data.username}
    //           onChange={handleInputChange}
    //           name="username"
    //           id="username" aria-describedby="emailHelp" placeholder="Username" required />
    //       </div>
    //       {/* <label htmlFor="username">
    //         Username
    //           <input
    //           type="text"
    //           value={data.username}
    //           onChange={handleInputChange}
    //           name="username"
    //           id="username"
    //         />
    //       </label> */}
    //       {/* 
    //       <label htmlFor="password">
    //         Password
    //           <input
    //           type="password"
    //           value={data.password}
    //           onChange={handleInputChange}
    //           name="password"
    //           id="password"
    //         />
    //       </label> */}

    //       <div class="form-group">
    //         <label className="font-weight-bold">Password</label>
    //         <input type="password" class="form-control pl-2 " value={data.password}
    //           onChange={handleInputChange}
    //           name="password"
    //           id="password" placeholder="Password"
    //           required />
    //       </div>

    //       <div class="form-check">
    //         <input type="checkbox" class="form-check-input mb-3" id="exampleCheck1"/>
    //           <label class="form-check-label" for="exampleCheck1">Check me out</label>
    //       </div>

    //         {data.errorMessage && (
    //           <span className="form-error mb-2">{data.errorMessage}</span>
    //         )}

    //         <button className="button-1 btn-outline-success mb-3" disabled={data.isSubmitting}>
    //           {data.isSubmitting ? (
    //             "Loading..."
    //           ) : (
    //               "Login"
    //             )}
    //         </button>
    //         <button className="button-1 btn-outline-primary" onClick={() => {
    //           setRegisterState(true)
    //         }}>
    //           Register
    //         </button>

    //     </form> :

    //       <>

    //   <Register />
    //   <button className="button-1 btn-outline-danger form-control" style={{'marginTop':'15%'}} onClick={() => {
    //     setRegisterState(false)
    //   }}>
    //     Back to login
    //   </button>
    // </>
    //     }
    //   </div>
    // </div>
    <div class="container-fluid">
      <div class="row no-gutter">
        <div class="d-none d-md-flex justify-content-center align-items-center col-md-4 col-lg-6 bg-image shadow-lg"><h1 style={{ 'color': 'white' }}>PTasks</h1></div>
        <div class="col-md-8 col-lg-6">
          <div class="login d-flex align-items-center py-5">
            <div class="container">
              <div class="row">
                <div class="col-md col-lg">
                  {!registerState ? <form onSubmit={handleFormSubmit}>
                  <h3 class="login-heading mb-4">Welcome back!</h3>
                    <div class="form-label-group">
                      <input type="text" class="form-control" placeholder="Username" required autofocus
                        value={data.username}
                        onChange={handleInputChange}
                        name="username"
                        id="username"
                      />
                      <label htmlFor="username">Username</label>
                    </div>
                    <div class="form-label-group">
                      <input type="password" class="form-control" placeholder="Password" required
                        value={data.password}
                        onChange={handleInputChange}
                        name="password"
                        id="password" />
                      <label htmlFor="password">Password</label>
                    </div>
                    <div class="custom-control custom-checkbox mb-3">
                      <input type="checkbox" class="custom-control-input" id="customCheck1" />
                      <label class="custom-control-label" for="customCheck1">Remember password</label>
                    </div>
                    <div className="d-flex align-items-center justify-content-center">
                    {data.errorMessage && (
                      <span className="form-error mb-2">{data.errorMessage}</span>
                    )}
                    </div>
                    <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" disabled={data.isSubmitting}>{data.isSubmitting ? (
                      "Loading..."
                    ) : (
                        "Sign In"
                      )}</button>
                    <div class="text-center">
                      <button className="btn btn-lg btn-danger btn-block btn-login text-uppercase font-weight-bold" onClick={() => {
                        setRegisterState(true)
                      }}>
                        Register
                      </button></div>
                  </form> :
                    <>
                      <Register />
                      <button className="btn btn-lg btn-danger btn-block btn-login text-uppercase font-weight-bold" style={{ 'marginTop': '15%' }} onClick={() => {
                        setRegisterState(false)
                      }}>
                        Back to login
                      </button>
                    </>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Login;