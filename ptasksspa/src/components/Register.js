import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { apiUrl } from "../env"
import { AuthContext } from "../App";


export default function Register() {
  const { dispatch } = React.useContext(AuthContext);


  const register = async (fields) => {
    await fetch(`${apiUrl}/register`, {
      method: 'POST',
      headers: {
        "accept": "application/json",
        "content-type": "application/json"
      },
      body: JSON.stringify(
        fields
      )
    }).then(res => {
      if (res.ok) {
        return res.json().then(resJson => {
          dispatch({
            type: "LOGIN",
            payload: resJson

          })
        });
      }
      if (!res.ok) {
        return res.json().then(text => alert(text?.message))
      }

    })
      .catch(error => {
        console.log("Register -> error", error)

      });
  }



  return (
    <Formik
      initialValues={{
        username: '',
        password: '',
        confirmPassword: ''
      }}
      validationSchema={Yup.object().shape({

        username: Yup.string()
          .min(4, 'Username must be at least 4 characters')
          .max(20, 'Username must be less than 20 characters')
          .required('Username is required'),
        password: Yup.string()
          .min(6, 'Password must be at least 6 characters')
          .max(20, 'Password must be less than 20 characters')
          .required('Password is required'),
        confirmPassword: Yup.string()
          .oneOf([Yup.ref('password'), null], 'Passwords must match')
          .required('Confirm Password is required')
      })}
      onSubmit={fields => {
        // console.log(fields)
        // alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields))
        register(fields);
      }}
      render={({ errors, status, touched }) => (
        <Form>
          <h3 class="login-heading mb-4 text-center">Sign Up</h3>
          {/* <div className="mt-5">
          <h3 className="mt-5" align="center">Register</h3>
            <div className="form-group">
              <label htmlFor="username" className="font-weight-bold">Username</label>
              <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
              <ErrorMessage name="username" component="div" className="invalid-feedback" />
            </div>
            <div className="form-group">
              <label htmlFor="password" className="font-weight-bold">Password</label>
              <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
              <ErrorMessage name="password" component="div" className="invalid-feedback" />
            </div>
            <div className="form-group">
              <label htmlFor="confirmPassword" className="font-weight-bold">Confirm Password</label>
              <Field name="confirmPassword" type="password" className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
              <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
            </div>
            <div className="form-group">
              <button type="submit" className="button-1 btn-outline-success mr-2 form-control" style={{ 'marginBottom': '35%' }}>Register</button>
            </div>
          </div> */}
          <div class="form-label-group">
            {/* <input type="text" id="inputUserame" class="form-control" placeholder="Username" required autofocus /> */}
            <Field name="username" id="inputUsername" type="text" placeholder="Username" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
            <label htmlFor="username" for="inputUsername">Username</label>
            <ErrorMessage name="username" component="div" className="invalid-feedback" />
          </div>
          <hr />

          <div class="form-label-group">
            {/* <input type="password" id="inputPassword" class="form-control" placeholder="Password" required /> */}
            <Field className={"form-control" + (errors.password && touched.password ? ' is-invalid' : '')} name="password" id="inputPassword" type="password" placeholder="Password" />
            <label htmlFor="password" for="inputPassword">Password</label>
            <ErrorMessage name="password" component="div" className="invalid-feedback" />
          </div>

          <div class="form-label-group">
            {/* <input type="password" id="inputConfirmPassword" class="form-control" placeholder="Password" required />
            <label for="inputConfirmPassword">Confirm password</label> */}
            <Field name="confirmPassword" id="inputConfirmPassword" type="password" placeholder="Password" className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
            <label htmlFor="confirmPassword" for="inputConfirmPassword">Confirm password</label>
            <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
          </div>

          <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
          <hr class="my-4"/>
        </Form>
      )}
    />
  )

}
