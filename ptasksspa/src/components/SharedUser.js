import React from "react";
import { apiUrl } from "../env"
import { useParams } from "react-router-dom";

export default function SharedUser({ authToken, catename }) {

    const [users, setUsers] = React.useState([])
    let { id } = useParams();



    const getSharedUsers = async () => {
        await fetch(`${apiUrl}/categories/${id}/users`, {
            method: "get",
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${authToken}`
            }
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw res;
                }
            })
            .then(resJson => {
                console.log("SharedUser -> resJson", resJson)
                setUsers(resJson)
            })
            .catch(error => {
                console.log(error);
            });
    }

    const removeUser = async (userId) => {
        await fetch(`${apiUrl}/categories/${id}/users?userId=${userId}`, {
            method: "delete",
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${authToken}`
            }
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw res;
                }
            })
            .then(resJson => {
            })
            .catch(error => {
                console.log(error);
            });
    }

    const updateUser = async (userId, value) => {
        await fetch(`${apiUrl}/categories/${id}/users?userId=${userId}&permission=${value}`, {
            method: "put",
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${authToken}`
            }

        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw res;
                }
            })
            .then(resJson => {
            })
            .catch(error => {
                console.log(error);
            });
    }

    const onSelectedChange = (userId, event) => {
        updateUser(userId, event.target.value)
    }

    React.useEffect(() => {

        getSharedUsers()


    }, [])
    return (
        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h1 className="h3">{catename}</h1>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Current permission</th>
                        <th scope="col">Handle</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user, index) => (
                        < tr >
                            <th scope="row">{index}</th>
                            <td>{user.username}</td>
                            <td>
                                {/* {user.crud_permission == "rw" ? "Read & Write" : "Read"}  */}
                                <select class="form-control" onChange={(event) => { onSelectedChange(user.id, event) }}>
                                    <option value="rx" selected={user.crud_permission === "rx"}>Read</option>
                                    <option value="rw" selected={user.crud_permission === "rw"}>Read and Write</option>
                                </select></td>
                            <td>
                                <button className="btn btn-danger ml-1" onClick={() => { removeUser(user.id) }}>Remove</button>
                            </td>
                        </tr>
                    ))}
                    {/* <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td colspan="2">Larry the Bird</td>
                            <td>@twitter</td>
                        </tr> */}
                </tbody>
            </table>
        </main >

    );
}