import React/* , { useRef } */ from "react";
// import { Formik, Field, Form, ErrorMessage } from 'formik';
// import * as Yup from 'yup';
import { apiUrl } from "../env"
import moment from 'moment'
import "../assets/tasks.css"
import { Modal, Button } from 'react-bootstrap';
import { Editor } from '@tinymce/tinymce-react';
import { useParams } from "react-router-dom";
import 'antd/dist/antd.css';
import { DatePicker, message, Drawer } from 'antd';
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import ScaleLoader from "react-spinners/HashLoader";
import { css } from "@emotion/core";
import copy from "copy-to-clipboard";
import {

    Link,
} from "react-router-dom";

import Timeline from './Timeline'
// const override = css`
//   display: block;
//   margin: 2 auto;
//   border-color: red;
// `;

const { RangePicker } = DatePicker;

message.config({
    top: 65,
    duration: 2,
    maxCount: 3,
    rtl: true,
});



export default function Tasks({ authToken, searchString, catename }) {


    const { errors, handleSubmit, register, setValue, watch, } = useForm();
    let { id } = useParams();
    const history = useHistory();
    const [date, setDate] = React.useState([])
    const [fullDescription, setFullDescription] = React.useState("<p>Note describe...</p>")
    const [reverse, setReverse] = React.useState(false);
    const [reFetch, setReFetch] = React.useState(false);
    const [showCreate, setShowCreate] = React.useState(false);
    const [showEdit, setShowEdit] = React.useState(false);
    const [showDelete, setShowDelete] = React.useState(false);
    const [tasks, setTasks] = React.useState([]);
    const [taskId, setTaskId] = React.useState(1); // selected Task id
    const [sharedCode, setSharedCode] = React.useState({});
    const [permission, setPermission] = React.useState("");
    const [loading, setLoading] = React.useState(false)

    //start of filter init
    const [initFilter, setInitFilter] = React.useState([
        { "name": "All", "value": "all" },
        { "name": "This week", "value": "sortByWeek" },
        { "name": "This month", "value": "sortByMonth" },
        { "name": "Custom range", "value": "sortByCustomRange" },
        { "name": "Expired", "value": "expired" },
    ]);


    const [selectedFilter, setSelectedFilter] = React.useState("");
    const [customRangeFilter, setCustomRangeFilter] = React.useState([]);
    //end of filter init

    function disabledDate(current) {
        return current && current < moment().startOf('day');
    }

    const handleShowCreate = () => {
        setShowCreate(true)
    };

    const handleShowEdit = (taskId) => {
        setTaskId(taskId)

        setShowEdit(true)


    };

    const assignEditValue = () => {
        let task = tasks.filter(task =>
            task.id == taskId
        )
        setFullDescription(task[0].content)
        setValue("nameEdit", task[0].name);
        setValue("descriptionEdit", task[0].description);
        setValue("priority", task[0].priority);
        let dateFrom = moment(task[0].date_from)
        let dateTo = moment(task[0].date_to)
        setDate([dateFrom, dateTo])

    }

    const handleShowDelete = (taskId) => {
        setTaskId(taskId)
        setShowDelete(true)
    };

    const handleCancelCreate = e => {
        setShowCreate(false)
    };

    const handleCancelDelete = () => {
        setShowDelete(false);
    }

    const handleCancelEdit = () => {
        setShowEdit(false);
    }

    const handleCreate = data => {
        let taskTemp = {};

        taskTemp["name"] = data.name;
        taskTemp["priority"] = data.priority;
        taskTemp["description"] = data.description;
        taskTemp["content"] = fullDescription;
        if (date.length > 0) {
            taskTemp["date_from"] = new Date(date[0]._d).toISOString().slice(0, 19).replace('T', ' ');
            taskTemp["date_to"] = new Date(date[1]._d).toISOString().slice(0, 19).replace('T', ' ');
        }
        else {
            taskTemp["date_from"] = new Date(Date.now()).toISOString().slice(0, 19).replace('T', ' ');
            taskTemp["date_to"] = new Date(Date.now() + 7).toISOString().slice(0, 19).replace('T', ' ');
        }



        addTask(taskTemp)
        setReFetch(!reFetch)
        setShowCreate(false)
    }

    const handleEdit = data => {
        let taskTemp = {};

        taskTemp["id"] = taskId;
        taskTemp["name"] = data.nameEdit;
        taskTemp["priority"] = data.priority;
        taskTemp["description"] = data.descriptionEdit;
        taskTemp["content"] = fullDescription;
        taskTemp["date_from"] = new Date(date[0]._d).toISOString().slice(0, 19).replace('T', ' ');
        taskTemp["date_to"] = new Date(date[1]._d).toISOString().slice(0, 19).replace('T', ' ');

        editTask(taskTemp)
        setTasks(prevState => {
            const task = prevState.filter(task => task.id == taskId);
            let index = prevState.indexOf(task[0]);
            taskTemp["created_by_uname"]=task[0].created_by_uname;
            prevState[index] = taskTemp;
            return prevState;
        });
        setShowEdit(false)
    }

    const handleDelete = () => {
        deleteTask(taskId)
        setTasks(prevState => {
            const tasks = prevState.filter(task => task.id !== taskId);
            return tasks;
        });
        setShowDelete(false);
    }

    const getCate = async () => {
        await fetch(`${apiUrl}/categories/${id}`, {
            method: "get",
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${authToken}`
            }
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw res;
                }
            })
            .then(resJson => {
                if (resJson.length > 0) {

                    setSharedCode(resJson[0].shared_code)
                    if (JSON.parse(localStorage.getItem('user')).id == resJson.user_id)

                        setPermission("owner")
                    else
                        setPermission(resJson[0].crud_permission)
                }

                else {
                    setSharedCode(resJson.shared_code)
                    if (JSON.parse(localStorage.getItem('user')).id == resJson.user_id)

                        setPermission("owner")
                    else
                        setPermission(resJson[0].crud_permission)
                }
            })
            .catch(error => {
                console.log(error);
            });
    }


    const getTasks = async () => {
        setLoading(true)
        let dateFrom;
        let dateTo;
        if (customRangeFilter.length > 0 && selectedFilter === "sortByCustomRange") {
            dateFrom = new Date(customRangeFilter[0]._d).toISOString().slice(0, 19).replace('T', ' ');
            dateTo = new Date(customRangeFilter[1]._d).toISOString().slice(0, 19).replace('T', ' ');
        }
        else {
            dateFrom = ""
            dateTo = ""
        }
        await fetch(`${apiUrl}/${id}/tasks?${selectedFilter}=true&dateFrom=${dateFrom}&dateTo=${dateTo}`, {
            method: "get",
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${authToken}`
            }
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    history.push("/");
                    throw res;
                }
            })
            .then(resJson => {
                setLoading(false)
                setTasks(resJson)

            })
            .catch(error => {
                console.log(error);
            });
    }

    const addTask = async (data) => {
        await fetch(`${apiUrl}/${id}/tasks`, {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${authToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => {
                if (res.ok) {
                    message.success('Ok');
                    return res.json();

                } else {
                    throw res;
                }
            })
            .then(resJson => {
                console.log("Tasks -> resJson", resJson)
            })
            .catch(error => {
                console.log(error);
            });

    }

    const editTask = async (data) => {
        await fetch(`${apiUrl}/${id}/tasks?taskid=${taskId}`, {
            method: "PUT",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${authToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => {
                if (res.ok) {
                    message.success('Ok');

                    return res.json();

                } else {
                    throw res;
                }
            })
            .then(resJson => {
                console.log("Task -> resJson", resJson);
            })
            .catch(error => {
                console.log(error);
            })
    }

    const deleteTask = async (taskid) => {
        await fetch(`${apiUrl}/${id}/tasks?taskid=${taskid}`, {
            method: "DELETE",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${authToken}`
            },
        })
            .then(res => {
                if (res.ok) {
                    message.success('Ok');
                    return res.json();
                } else {
                    throw res;
                }
            })
            .then(resJson => {
                console.log("Tasks -> resJson", resJson);
            })
            .catch(error => {
                console.log(error);
            });
    }

    React.useEffect(() => {
        setTasks([])
        getTasks();
        getCate();

    }, [id, reFetch])

    function createMarkup(html) {
        return { __html: html };
    }

    const filterData = () => {
        setReFetch(!reFetch)
    }
    const [drawer, setDrawer] = React.useState(false)

    return (
        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">

            <Timeline show={drawer} close={() => { setDrawer(false) }} tasks={tasks} />
            {/* modal add tasks */}

            <Modal
                size="lg"
                centered
                animation={false}
                show={showCreate}
                onHide={handleCancelCreate}>
                <Modal.Header closeButton>
                    <Modal.Title>Adding a new task</Modal.Title>
                </Modal.Header>
                <form onSubmit={handleSubmit(handleCreate)}>
                    <Modal.Body >
                        <div className="my-custom-scrollbar-1">

                            <div className="form-group">
                                <label htmlFor="name"><strong>Title</strong></label>
                                <input name="name" type="text" className={'form-control' + (errors.name ? ' is-invalid' : '')} ref={register({ required: true })} />
                                {
                                    errors.name &&
                                    <div name="name" className="invalid-feedback" >
                                        Required
                                    </div>
                                }

                            </div>
                            <div className="form-group">
                                <label htmlFor="description"><strong>Short description</strong></label>
                                <input name="description" type="text" className={'form-control' + (errors.description ? ' is-invalid' : '')} ref={register({ required: true })} />

                                {
                                    errors.description &&
                                    <div name="description" className="invalid-feedback" >
                                        Required
                                </div>
                                }

                            </div>

                            <div className="form-group">
                                <label htmlFor="content"><strong>Full description</strong></label>
                                <Editor
                                    apiKey="ags64nlq2hxdjivptjss7qdyf1e3n50mtct3ueye00e54c8a"
                                    name="content"
                                    onEditorChange={setFullDescription}
                                    value={fullDescription}
                                    className={'form-control' + (errors.content ? ' is-invalid' : '')}
                                    // initialValue="<p>This is the initial content of the editor</p>"
                                    init={{
                                        height: 200,
                                        menubar: false,
                                        entity_encoding: "raw",
                                        verify_html: false,
                                        plugins: [
                                            'advlist autolink lists link image charmap print preview anchor',
                                            'searchreplace visualblocks code fullscreen',
                                            'insertdatetime media table paste code help wordcount'
                                        ],
                                        toolbar:
                                            'undo redo | formatselect | bold italic backcolor forecolor | \
                                        alignleft aligncenter alignright alignjustify | \
                                        bullist numlist outdent indent | removeformat | help preview sizeselect fontselect fontsizeselect'
                                    }}
                                />

                            </div>
                            <div className="form-group">
                                <label htmlFor="date"><strong>Date from (default is 1 week)</strong></label>
                                <RangePicker
                                    name="date"
                                    size="large"
                                    defaultValue={date}
                                    disabledDate={disabledDate}
                                    value={date}
                                    onChange={setDate}
                                    className={'form-control' + (errors.date ? ' is-invalid' : '')}
                                    // showTime
                                />
                                {
                                    errors.date &&
                                    <div name="description" className="invalid-feedback" >
                                        Required
                                </div>
                                }
                            </div>
                            <div className="form-group">
                                <label htmlFor="priority"><strong>Priority</strong></label>

                                <select name="priority" className={'form-control' + (errors.priority ? ' is-invalid' : '')} ref={register({ required: true })} >
                                    <option value="low">Low</option>
                                    <option value="medium">Medium</option>
                                    <option value="high">High</option>
                                </select>
                                {errors.priority && <div name="priority" className="invalid-feedback" >
                                    Required
                                </div>}
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <input type="submit" /> */}
                        <Button variant="secondary" onClick={handleCancelCreate}>
                            Close
                        </Button>
                        <Button variant="primary" type="submit" /* onClick={handleCreate} */>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            {/* modal edit tasks */}
            <Modal
                size="lg"
                centered
                animation={false}
                show={showEdit}
                onShow={() => assignEditValue()}
                onHide={handleCancelEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>Editing task</Modal.Title>
                </Modal.Header>
                <form onSubmit={handleSubmit(handleEdit)}>
                    <Modal.Body>
                        <div className="my-custom-scrollbar-1">

                            <div className="form-group">
                                <label htmlFor="name"><strong>Title</strong></label>
                                <input
                                    name="nameEdit"
                                    type="text"
                                    ref={register({ required: true })}
                                    className={'form-control' + (errors.nameEdit ? ' is-invalid' : '')} />
                                {
                                    errors.nameEdit &&
                                    <div name="nameEdit" className="invalid-feedback" >
                                        Required
                                </div>
                                }
                            </div>
                            <div className="form-group">
                                <label htmlFor="description"><strong>Short description</strong></label>
                                <input name="descriptionEdit" type="text" className={'form-control' + (errors.descriptionEdit ? ' is-invalid' : '')} ref={register({ required: true })} />

                                {
                                    errors.descriptionEdit &&
                                    <div name="descriptionEdit" className="invalid-feedback" >
                                        Required
                                </div>
                                }

                            </div>

                            <div className="form-group">
                                <label htmlFor="content"><strong>Full description</strong></label>
                                <Editor
                                    apiKey="ags64nlq2hxdjivptjss7qdyf1e3n50mtct3ueye00e54c8a"
                                    name="content"

                                    onEditorChange={setFullDescription}
                                    value={fullDescription}
                                    className={'form-control' + (errors.content ? ' is-invalid' : '')}
                                    init={{
                                        height: 200,
                                        menubar: false,
                                        entity_encoding: "raw",
                                        verify_html: false,
                                        // plugins: [
                                        //     'advlist autolink lists link image charmap print preview anchor',
                                        //     'searchreplace visualblocks code fullscreen',
                                        //     'insertdatetime media table paste code help wordcount'
                                        // ],
                                        toolbar:
                                            'undo redo | formatselect | bold italic backcolor forecolor| \
                                        alignleft aligncenter alignright alignjustify | \
                                        bullist numlist outdent indent | removeformat | help sizeselect fontselect fontsizeselect '
                                    }}
                                />

                            </div>
                            <div className="form-group">
                                <label htmlFor="date"><strong>Date from</strong></label>
                                <RangePicker
                                    ref={register({ required: true })}
                                    name="date"
                                    size="large"
                                    disabledDate={disabledDate}
                                    value={date}
                                    onChange={setDate}
                                    className={'form-control' + (errors.date ? ' is-invalid' : '')}
                                    // showTime
                                />
                                {
                                    errors.date &&
                                    <div name="description" className="invalid-feedback" >
                                        Required
                                </div>
                                }
                            </div>

                            <div className="form-group">
                                <label htmlFor="priority"><strong>Priority</strong></label>

                                <select name="priority" className={'form-control' + (errors.priority ? ' is-invalid' : '')} ref={register({ required: true })} >
                                    <option value="low">Low</option>
                                    <option value="medium">Medium</option>
                                    <option value="high">High</option>
                                </select>
                                {
                                    errors.priority &&
                                    <div name="priority" className="invalid-feedback" >
                                        Required
                                </div>
                                }
                            </div>



                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleCancelEdit}>
                            Close
                        </Button>
                        <Button variant="primary" type="submit">
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            {/* modal delete tasks */}
            <Modal show={showDelete} onHide={handleCancelDelete} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Remove this task</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure to remove this?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCancelDelete}>
                        No
            </Button>
                    <Button variant="primary" onClick={handleDelete}>
                        Yes, remove it
            </Button>
                </Modal.Footer>
            </Modal>

            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <div className="form-inline">
                    <h1 className="h3 mr-1"><svg class="bi bi-calendar2-minus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M5.5 10.5A.5.5 0 0 1 6 10h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z" />
                        <path fill-rule="evenodd" d="M14 2H2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1zM2 1a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z" />
                        <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z" />
                        <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                    </svg> -</h1>
                    <h1 className="h3">{catename}</h1></div>
                <div className="btn-toolbar mb-2 mb-md-0">


                    <button className="btn btn-primary ml-2" onClick={() => { setDrawer(true) }}> Timeline</button>
                    {permission === "owner" && <> <Link to={'/users/' + id + '?catename=' + catename} className="btn btn-outline-secondary ml-2"><svg class="svg-icon" viewBox="0 0 20 20">
                        <path d="M12.075,10.812c1.358-0.853,2.242-2.507,2.242-4.037c0-2.181-1.795-4.618-4.198-4.618S5.921,4.594,5.921,6.775c0,1.53,0.884,3.185,2.242,4.037c-3.222,0.865-5.6,3.807-5.6,7.298c0,0.23,0.189,0.42,0.42,0.42h14.273c0.23,0,0.42-0.189,0.42-0.42C17.676,14.619,15.297,11.677,12.075,10.812 M6.761,6.775c0-2.162,1.773-3.778,3.358-3.778s3.359,1.616,3.359,3.778c0,2.162-1.774,3.778-3.359,3.778S6.761,8.937,6.761,6.775 M3.415,17.69c0.218-3.51,3.142-6.297,6.704-6.297c3.562,0,6.486,2.787,6.705,6.297H3.415z"></path>
                    </svg></Link>

                        <button type="button" className="btn btn-outline-secondary ml-1" data-toggle="tooltip" data-placement="left" title="Copy to clipboard (shared code)" onClick={() => {

                            copy(sharedCode);

                        }}><svg class="bi bi-clipboard my-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                                <path fill-rule="evenodd" d="M9.5 1h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                            </svg></button></>}





                    <div className="col-auto my-1">
                        <label className="mr-sm-2 sr-only" htmlFor="inlineFormCustomSelect">Preference</label>
                        <select className="custom-select mr-sm-2" id="inlineFormCustomSelect" onChange={(e) => {
                            setSelectedFilter(e.target.value)

                        }}>
                            {initFilter.map((value) => (
                                <option value={value.value}>{value.name}</option>
                            ))}

                        </select>
                    </div>






                    {selectedFilter === "sortByCustomRange" && <RangePicker onChange={setCustomRangeFilter} />}

                    <button className="btn btn-primary ml-2" onClick={() => { filterData() }}><svg class="bi bi-filter" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
                    </svg> Filter</button>
                    {
                        (permission === "rw" || permission === "owner") &&
                        <button type="button" className="btn btn-outline-secondary ml-2" onClick={() => {

                            handleShowCreate()

                        }}>Add a new task</button>
                    }

                </div>

            </div>

            <div className="my-custom-scrollbar-2">
                <div className="sweet-loading" align="center">
                    <ScaleLoader
                        size={50}
                        width={10}
                        height={45}
                        radius={5}
                        color={"#f7310a"}
                        loading={loading}
                    />
                </div>
                {tasks.length == 0 && loading == false && <div className="text-center" align="center"><h2>No results found !!!</h2></div>}

                <div className='row'>
                    <div className='col1 pl-3'>
                        {tasks.length > 0 &&
                            tasks.map(task => {
                                if (task.priority == "high") return (
                                    <div className={'mb-3 card'} key={task.id}>
                                        <h5 className={"card-header  " + (task.priority == "high" ? 'bg-danger text-white' : "")
                                            + (task.priority == "medium" ? 'bg-warning text-white' : 'bg-success text-white')}>
                                            {task.name} (from {task.created_by_uname})</h5>
                                        <div className="card-body">
                                            <div className="mb-3">
                                                <p className="card-text">{task.description}</p>
                                            </div>
                                            <div>
                                                <button className="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target={'#' + task.id} aria-expanded="false" aria-controls={task.id}>Details</button>
                                                {

                                                    (permission === "rw" || permission === "owner") && <><button className="btn btn-outline-success ml-2 btn-sm" onClick={() => { handleShowEdit(task.id) }}>Edit</button>
                                                        <button className="btn btn-outline-danger ml-2 btn-sm" onClick={() => { handleShowDelete(task.id) }}>Remove Task</button>
                                                    </>
                                                }
                                            </div>
                                            <div className="collapse" id={task.id}>
                                                <div className="card-header mt-3 bg-light border" dangerouslySetInnerHTML={createMarkup(task.content)} >
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer bg-transparent border-secondary">
                                            {moment(task.date_from).format('LL')}
                                        &nbsp;- {moment(task.date_to).format('LL')}&nbsp;<br/>(Updated at: {moment(task.updated_at).format('LL')})
                                    </div>
                                        <div className="card-footer bg-transparent">
                                            {/* <strong> {task.days_left > 0 && task.days_left + ' days left from today'} {task.days_left < 0 && 'Expired'} {task.days_left == 0 && 'This task will expire today'}</strong> */}
                                            <strong> Deadline: {moment(task.date_to).fromNow()}</strong>


                                        </div>
                                    </div>
                                )
                            })}
                    </div>
                    <div className='col1'>
                        {tasks.length > 0 &&
                            tasks.map(task => {
                                if (task.priority == "medium") return (
                                    <div className={'mb-3 card'} key={task.id}>
                                        <h5 className={"card-header " + (task.priority == "high" ? 'bg-danger text-white' : "")
                                            + (task.priority == "medium" ? 'bg-warning text-white' : 'bg-success text-white')}>
                                            {task.name} (from {task.created_by_uname})</h5>
                                        <div className="card-body">
                                            <div className="mb-3">
                                                <p className="card-text">{task.description}</p>
                                            </div>
                                            <div>
                                                <button className="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target={'#' + task.id} aria-expanded="false" aria-controls={task.id}>Details</button>
                                                {

                                                    (permission === "rw" || permission === "owner") && <><button className="btn btn-outline-success ml-2 btn-sm" onClick={() => { handleShowEdit(task.id) }}>Edit</button>
                                                        <button className="btn btn-outline-danger ml-2 btn-sm" onClick={() => { handleShowDelete(task.id) }}>Remove Task</button>
                                                    </>
                                                }
                                            </div>
                                            <div className="collapse" id={task.id}>
                                                <div className="card-header mt-3 bg-light border" dangerouslySetInnerHTML={createMarkup(task.content)} >
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer bg-transparent border-secondary">
                                            {moment(task.date_from).format('LL')}
                                        &nbsp; - {moment(task.date_to).format('LL')}&nbsp;<br/>(Updated at: {moment(task.updated_at).format('LL')})
                                    </div>
                                        <div className="card-footer bg-transparent">
                                            {/* <strong> {task.days_left > 0 && task.days_left + ' days left from Today'} {task.days_left < 0 && 'Expired'} {task.days_left == 0 && 'This task will expire today'}</strong> */}
                                            <strong> Deadline: {moment(task.date_to).fromNow()}</strong>


                                        </div>
                                    </div>
                                )
                            })}
                    </div>
                    <div className='col1 pr-1'>
                        {tasks.length > 0 &&
                            tasks.map(task => {
                                if (task.priority == "low") return (
                                    <div className={'mb-3 card'} key={task.id}>
                                        <h5 className={"card-header " + (task.priority == "high" ? 'bg-danger text-white' : "")
                                            + (task.priority == "medium" ? 'bg-warning text-white' : 'bg-success text-white')}>
                                            {task.name} (from {task.created_by_uname})</h5>
                                        <div className="card-body">
                                            <div className="mb-3">
                                                <p className="card-text">{task.description}</p>
                                            </div>
                                            <div>
                                                <button className="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target={'#' + task.id} aria-expanded="false" aria-controls={task.id}>Details</button>
                                                {

                                                    (permission === "rw" || permission === "owner") && <><button className="btn btn-outline-success ml-2 btn-sm" onClick={() => { handleShowEdit(task.id) }}>Edit</button>
                                                        <button className="btn btn-outline-danger ml-2 btn-sm" onClick={() => { handleShowDelete(task.id) }}>Remove Task</button>
                                                    </>
                                                }
                                                {/* <button className="btn btn-outline-danger ml-2 btn-sm" >Delete</button> */}

                                            </div>
                                            <div className="collapse" id={task.id}>
                                                <div className="card-header mt-3 bg-light border" dangerouslySetInnerHTML={createMarkup(task.content)} >
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer bg-transparent border-secondary">
                                            {moment(task.date_from).format('LL')}
                                        &nbsp;- {moment(task.date_to).format('LL')}&nbsp;<br/>(Updated at: {moment(task.updated_at).format('LL')})
                                    </div>
                                        <div className="card-footer bg-transparent">
                                            {/* <strong> {task.days_left > 0 && task.days_left + ' days left from Today'} {task.days_left < 0 && 'Expired'} {task.days_left == 0 && 'This task will expire today'}</strong> */}
                                            <strong> Deadline: {moment(task.date_to).fromNow()}</strong>

                                        </div>
                                    </div>
                                )
                            })}
                    </div>
                </div>
            </div>



        </main>

    )
}



