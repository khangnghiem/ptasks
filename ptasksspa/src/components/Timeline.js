import React from "react";
import { DatePicker, message, Drawer, Timeline } from 'antd';
import moment from 'moment'


export default function App({ show, close, tasks }) {

    const expired = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill-rule="evenodd" d="M16.168 2.924L4.51 13.061a.25.25 0 00.164.439h5.45a.75.75 0 01.692 1.041l-2.559 6.066 11.215-9.668a.25.25 0 00-.164-.439H14a.75.75 0 01-.687-1.05l2.855-6.526zm-.452-1.595a1.341 1.341 0 012.109 1.55L15.147 9h4.161c1.623 0 2.372 2.016 1.143 3.075L8.102 22.721a1.149 1.149 0 01-1.81-1.317L8.996 15H4.674c-1.619 0-2.37-2.008-1.148-3.07l12.19-10.6z"></path></svg>';

 
    return (
        <>
            <Drawer
                title="Drawer"
                placement="right"
                width={400}
                closable={false}
                onClose={() => { close() }}
                visible={show}
            >
                <h4 className="mb-3">Timeline view</h4>
                <Timeline>
                    {tasks.map(task => {
                        let color;
                        if (task.priority === "high")
                            color = "red"
                        if (task.priority === "medium")
                            color = "orange"
                        if (task.priority === "low")
                            color = "green"
                        return (
                            <Timeline.Item color={color}>
                                <p ><strong>{task.name}</strong> - Deadline: {moment(task.date_to).format('L')} -
                                    {new Date(task.date_to) < Date.now() ? <span
                                        dangerouslySetInnerHTML={{ __html: expired }}></span> : ""}</p>
                                <p >{task.description}</p>
                            </Timeline.Item>
                        )

                    })}
                    {/* <Timeline.Item color="green">Create a services site 2015-09-01</Timeline.Item>
                    <Timeline.Item color="green">Create a services site 2015-09-01</Timeline.Item>
                    <Timeline.Item color="red">
                        <p>Solve initial network problems 1</p>
                        <p>Solve initial network problems 2</p>
                        <p>Solve initial network problems 3 2015-09-01</p>
                    </Timeline.Item>
                    <Timeline.Item>
                        <p>Technical testing 1</p>
                        <p>Technical testing 2</p>
                        <p>Technical testing 3 2015-09-01</p>
                    </Timeline.Item>
                    <Timeline.Item color="gray">
                        <p>Technical testing 1</p>
                        <p>Technical testing 2</p>
                        <p>Technical testing 3 2015-09-01</p>
                    </Timeline.Item>
                    <Timeline.Item color="gray">
                        <p>Technical testing 1</p>
                        <p>Technical testing 2</p>
                        <p>Technical testing 3 2015-09-01</p>
                    </Timeline.Item> */}
                </Timeline>
            </Drawer>
        </>

    );
}